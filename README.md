# Guestlist application based on Thorgate task
## Steps to start
```
$ sudo django-admin.py startproject guestlist
$ cd guestlist
$ sudo python manage.py migrate
$ sudo python manage.py runserver
$ sudo python manage.py shell
$ sudo python manage.py createsuperuser
$ sudo python manage.py startapp auth
$ sudo python manage.py startapp posts
$ sudo python manage.py startapp validation
```
```
$ sudo apt-get install python-mysqldb #Debian specific see more http://stackoverflow.com/questions/2952187/getting-error-loading-mysqldb-module-no-module-named-mysqldb-have-tried-pre
```
## Helper URLs
https://docs.djangoproject.com/en/1.7/intro/tutorial01/
https://docs.djangoproject.com/en/1.8/topics/auth/default/

## First structure in mind
guestlist
--posts
--auth
--validation

